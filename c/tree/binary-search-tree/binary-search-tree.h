/*
 * =====================================================================================
 *
 *       Filename:  binary-search-tree.h
 *
 *    Description:  declarations of binary search tree
 *
 *        Version:  1.0
 *        Created:  2017-03-19
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  luoyz , NULL
 *
 * =====================================================================================
 */

#ifndef _BINARY_SEARCH_TREE_H_
#define _BINARY_SEARCH_TREE_H_

#include "../../lib/type_rename.h"

typedef struct tree_node TNODE;

struct tree_node
{
	element_type data;
	TNODE        *left;
	TNODE        *right;
};

typedef struct tree_node *SEARCH_TREE;

#endif   /* _BINARY_SEARCH_TREE_H_ */

