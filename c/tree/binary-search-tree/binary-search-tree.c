/*
 * =====================================================================================
 *
 *       Filename:  binary-search-tree.c
 *
 *    Description:	implementations of binary search tree
 *
 *        Version:  1.0
 *        Created:  2017-03-19 19:32:52
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  luoyz , NULL
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>

#include "../../lib/common_def.h"
#include "binary-search-tree.h"

SEARCH_TREE make_empty(SEARCH_TREE t)
{
	if(t != NULL)
	{
		make_empty(t->left);
		make_empty(t->right);
		free(t);
	}

	return NULL;
}

TNODE *find_node(SEARCH_TREE t, element_type x)
{
	if(NULL == t)
	{
		return NULL;
	}
	if(x < t->data)
	{
		return find_node(t->left, x);
	}
	else if(x > t->data)
	{
		return find_node(t->right, x);
	}
	else
	{
		return t;
	}
}

TNODE *find_min(SEARCH_TREE t)
{
	if(t != NULL)
	{
		while(t->left != NULL)
		{
			t = t->left;
		}
	}

	return t;
}

TNODE *find_max(SEARCH_TREE t)
{
	if(t != NULL)
	{
		while(t->right != NULL)
		{
			t = t->right;
		}
	}

	return t;
}

/* Height of node
 * The height of a node is the number of edges on the longest path
 * between that node and a leaf.
 * The height of a tree is the height of its root node.
 * */
int node_height(TNODE *node)
{
	int height;
	int lh, rh;

	if(NULL == node)
	{
		/* For root node :
		 * conventionally, an empty tree (tree with no nodes,
		 * if such are allowed) has height −1. */
		return -1;
	}

	if(NULL == node->left && NULL == node->right)
	{
		return 0;
	}
	else
	{
		lh = node_height(node->left);
		rh = node_height(node->right);
		height = lh > rh ? lh : rh;
	}

	return height + 1;
}

SEARCH_TREE insert(SEARCH_TREE t, element_type x)
{
	if(NULL == t)
	{
		/* Create and return a one-node tree */
		t = (SEARCH_TREE)malloc(sizeof(TNODE));
		if(NULL == t)
		{
			perror("Out of memory!!!");
			return NULL;
		}
		else
		{
			t->data = x;
			t->left = t->right = NULL;
		}
	}
	else if(x < t->data)
	{
		t->left = insert(t->left, x);
	}
	else if(x > t->data)
	{
		t->right = insert(t->right, x);
	}
	else
	{
		/* x is in the tree already. We'll do nothing */
	}

	return t;
}

SEARCH_TREE delete(SEARCH_TREE t, element_type x)
{
	TNODE *temp;

	if(NULL == t)
	{
		perror("Element not found.");
		return t;
	}
	else if(x < t->data)
	{
		t->left = delete(t->left, x);
	}
	else if(x > t->data)
	{
		t->right = delete(t->right, x);
	}
	else if(t->left != NULL && t->right != NULL) /* Two children */
	{
		/* Replace with smallest in right subtree */
		temp = find_min(t->right);
		t->data = temp->data;
		t->right = delete(t->right, t->data);
	}
	else /* One or zero child */
	{
		temp = t;
		if(NULL == t->left)
		{
			t = t->right;
		}
		else if(NULL == t->right)
		{
			t = t->left;
		}
		free(temp);
	}

	return t;
}


int main(void)
{
	int nodes[10];
	uint32 i;
	SEARCH_TREE t = NULL;
	TNODE *node = NULL;
	element_type x;

	printf("Start testing binary search tree arithmetic: \n");
	srand(time(0));
	printf("The height of empty tree is %d\n", node_height(t));
	for(i = 0; i < 10; i++)
	{
		x = rand() % 10;
		t = insert(t, x);
		printf("After insert %d, the height of tree is %d\n", x, node_height(t));
	}
	printf("\n");
	node = find_min(t);
	if(node != NULL)
	{
		printf("The minimum element is %d\n", node->data);
		t = delete(t, node->data);
		printf("After delete minimum, the height of tree is %d\n", node_height(t));
	}
	node = find_max(t);
	if(node != NULL)
	{
		printf("The maximum element is %d\n", node->data);
		t = delete(t, node->data);
		printf("After delete maximum, the height of tree is %d\n", node_height(t));
	}

	node = find_node(t, 5);
	if(node != NULL)
	{
		printf("The height of this node is %d\n", node_height(node));
		t = delete(t, 5);
		printf("After delete 5, the height of tree is %d\n", node_height(t));
	}
	else
	{
		printf("Could not find a node with data 5\n");
	}

	make_empty(t);

	return 0;
}

